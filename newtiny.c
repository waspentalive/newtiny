
/*
** Read and parse source file
*/

/*
** First line of the program is a hard coded "[ $main ] @" jump to $main
** The user code should contain a label $main as its entry point
*/

/*
** Lines that start with # are comments or directives
** 
** This also allows for the first line of a tiny source code file to
** be a shebang #! line so a tiny program can be executed by

** #include filepath -- Read this file then return to main.
** ------- # ----------------------------------------------------------
** 0     0 0 1
** 1     7 9 1
** 
** #include tinylib/mylib
** label   c value or comment
**
**         c= : a label to jump to (or a string for self modify code
** =======   Labels are treated as constants that contain the line number
**           of the line they lie on. Line numbers start with 1 and step
**           by one for each line of code.
**
**         v= : A variable that can contain a string or a real
**         #  : A hash in any column marks that line as comment
**
** Source code 
*/

/*
** As source code is read each line is placed into the string part
** of a cell. The line number for each label is placed in the numeric field
** If there is a label an entry is made in the symbol table
**
** When the last line of code is read, the @ register is set to 1 and
** execution begins.
**
** Here is a simple hello world program
** ------- # ----------------------------------------------------------
** 0     0 0 1
** 1     7 9 1
** $main   : 
**           [10] count
** loop01  :
**           [" Hello World "] ?
**           [count] ?
**           ["\n"] ?
**           [count 1 -] count
**           [count 0 = ! loop01 *] @  #branch if not 0
**           ["Done \n"] ?
**           :
**
**
**    
*/
